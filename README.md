# material-dialogs

## Introduction
**material-dialogs** is a library for creating customizable dialogs in OpenHarmony applications.

![operation.gif](screenshots/operation.gif)

## How to Install
```shell
ohpm install @ohos/material-dialogs
```

## How to Use
1. Import **MaterialDialog** and code dependencies.
 ```
    import { MaterialDialog } from '@ohos/material_dialogs'
 ```
2. Initialize **Model()**.
 ```
   @State model: MaterialDialog.Model= new MaterialDialog.Model();

 ```
3. Initialize the dialog controller.
 ```
    dialogController: CustomDialogController = new CustomDialogController({
    builder: MaterialDialog({
      model: this.model
    }),
    cancel: this.existDialog,
    autoCancel: true,
    alignment: DialogAlignment.Center
    })
 ```

4. Call **model** functions to set and display dialogs.
 ```
    this.model.reset()
    this.model.message($r('app.string.useOhosLocationServicesPrompt'))
    this.model.positiveButton($r('app.string.agree'), {
     onClick() {
       console.info('ClickCallback when the confirm button is clicked')
     }
    })
    this.model.negativeButton($r('app.string.disagree'), {
     onClick() {
       console.info('ClickCallback when the cancel button is clicked')
     }
    })
    this.model.setScrollHeight(120)
    this.dialogController.open()
 ```

## Available APIs
`@State model: MaterialDialog.Model= new MaterialDialog.Model();`

| model.icon()                     | Description        |
| :-------------------------------- | :------------------ |
| model.icon()                     | Sets an icon for the dialog.          |
| model.title()                    | Sets the title of the dialog.          |
| model.message()                  | Sets the message of the dialog.      |
| model.positiveButton()           | Sets a positive action button, for example, **OK**.      |
| model.negativeButton()         | Sets a negative action button, for example, **Cancel**.      |
| model.neutralButton()          | Sets a neutral action button.      |
| model.setStacked()            | Sets whether to stack buttons.  |
| model.checkBoxPrompt()         | Sets a checkbox prompt for the dialog.        |
| model.setActionButtonEnabled() | Sets whether to enable an action button.  |
| model.listItems()              | Sets a list of items.      |
| model.listItemsSingleChoice()  | Sets a list of single-choice items.  |
| model.listItemsMultiChoice()   | Sets a list of multi-choice items.  |
| model.input()                  | Sets a text box.        |
| model.colorChooser()           | Sets a color picker.    |
| model.dateTimePicker()         | Sets a date/time picker.|



## Constraints
This project has been verified in the following versions:
- DevEco Studio: NEXT Beta1-5.0.3.806, SDK: API12 Release(5.0.0.66)

- DevEco Studio 4.1 Release(4.1.0.400), SDK: API11 (4.1.0.400)

- DevEco Studio: 4.0 Beta2(4.0.3.512), SDK: API10 (4.0.10.9)

- DevEco Studio: 4.0 Canary1(4.0.0.112), SDK: API10 (4.0.7.2)


## Directory Structure
````
|---- material-dialogs
|     |---- entry  # Sample code.
|     |---- material_dialogs  # material_dialogs library directory.
|           |---- index.ets  # API exposed externally.
|           |---- src
|                 |---- main
|                       |---- components
|                             |---- MaterialDialog.ets  # Main component class for creating and managing dialogs.
|                             |---- ClickCallback.ets   # Callbacks for handling click events for the dialog's buttons.
|                             |---- InputCallback.ets   # Callbacks for handling the text input changes.
|                             |---- ItemListener.ets    # APIs for handling item selection events. 
|     |---- README.md  # Introduction to material-dialogs and how to use it.
|     |---- README_zh.md  # Introduction to material-dialogs and how to use it.
````

## How to Contribute
If you find any problem during the use, submit an [issue](https://gitee.com/openharmony-sig/material-dialogs/issues) or [PR](https://gitee.com/openharmony-sig/material-dialogs/pulls) to us.

## License
This project is licensed under [Apache License 2.0](https://gitee.com/openharmony-sig/material-dialogs/blob/master/LICENSE).
